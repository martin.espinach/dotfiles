# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=-1
HISTFILESIZE=-1
HISTTIMEFORMAT=`echo -e "\u001B[38;5;33m%Y-%m-%d %T\033[0m "`
HISTIGNORE="history*:ls:hh*:ll"
HSTR_CONFIG="hicolor,raw-history-view"


# history convenience functions

chained-grep() {
    local pattern="$1"
    if [[ -z "$pattern" ]]; then
        cat
        return
    fi    

    shift
    grep --color=always "$pattern" | chained-grep "$@"
}

# ... Replaced by hstr
#
#hh() { 
#	if [ -z "$1" ]; then
#		history | sort -k 4 -u | sort -n | cut -c 42- 
#	else
#		history | sort -k 4 -u | sort -n | cut -c 42- | chained-grep "$@"
#	fi	
#}
alias hh="HSTR_CONFIG=hicolor,raw-history-view,prompt-bottom,help-on-opposite-side hstr"

hhs() {
	if [ -z "$1" ]; then
		history | sort -k 4 -u | sort -n | cut -c 41-
	else
		history | sort -k 4 -u | sort -n | cut -c 41- | chained-grep "$@"
	fi
}

hhl() {
	if [ -z "$1" ]; then
		history
	else
		history | chained-grep "$@"
	fi
}

hhu() {
	history -a; history -c; history -r
}

hhrd() {
	cp ~/.bash_history /tmp/bash_history
	sed -z -E 's/([0-9]{10})\n/\1 /g' /tmp/bash_history > /tmp/bash_history-1
	awk '(l=$0) && ($1=""); !_[$0]++ {print l}' /tmp/bash_history-1 > /tmp/bash_history-2
	sed -z -E 's/([0-9]{10}) /\1\n/g' /tmp/bash_history-2 > /tmp/bash_history-3
	cp /tmp/bash_history-3 ~/.bash_history
	rm -rf /tmp/bash_history*
	history -c; history -r
}

hhremove() {
	if [ ! -z "$1" ]; then
		local pattern="$1"
		shift
		local sedExpr=""
		local lines=$(cat ~/.bash_history | grep -n "$pattern" | chained-grep "$@" | cut -f1 -d:)
		for line in $lines; do
			local prev_line=$((${line}-1))
			sedExpr="${sedExpr};${prev_line}d;${line}d"
		done
		sed -i -e "$sedExpr" ~/.bash_history
		history -c; history -r
	fi
}

dckstp() {
	docker stop $(docker ps -q -f name=$1)
}

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

if command -v screen>/dev/null; then
  [[ ! $TERM =~ screen ]] && exec screen
fi

export GPGKEY=1276B4E5

export USE_CCACHE=1

# Rich console for gradle
alias gradle="gradle --console=rich"

# Pretty git log
alias gl="git log --pretty=format:\"%C(auto,yellow)%H%C(auto,magenta)% G? %C(auto,green)%<(20,trunc)%aN %C(auto,blue)%>(20,trunc)%ad %C(auto,reset)%s%C(auto,red)% gD% D\" --date=relative"
alias gg="git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue) <%an> %Creset' --abbrev-commit"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# added by pipsi (https://github.com/mitsuhiko/pipsi)
export PATH="/home/martin/.local/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"

# prompt
FMT_BOLD="\[\e[1m\]"
FMT_DIM="\[\e[2m\]"
FMT_RESET="\[\e[0m\]"
FMT_UNBOLD="\[\e[22m\]"
FMT_UNDIM="\[\e[22m\]"
FG_BLACK="\[\e[30m\]"
FG_BLUE="\[\e[34m\]"
FG_CYAN="\[\e[36m\]"
FG_GREEN="\[\e[32m\]"
FG_YELLOW="\[\e[33m\]"
FG_GREY="\[\e[37m\]"
FG_MAGENTA="\[\e[35m\]"
FG_RED="\[\e[31m\]"
FG_WHITE="\[\e[97m\]"
BG_BLACK="\[\e[40m\]"
BG_BLUE="\[\e[44m\]"
BG_CYAN="\[\e[46m\]"
BG_GREEN="\[\e[42m\]"
BG_YELLOW="\[\e[43m\]"
BG_MAGENTA="\[\e[45m\]"

PS1="\n${FG_BLUE}" # begin arrow to prompt
PS1+="${FG_MAGENTA}◢" # begin USERNAME container
PS1+="${BG_MAGENTA}${FG_WHITE} \u" # print username
PS1+="${FMT_UNBOLD}${FMT_BOLD} ${FG_MAGENTA}${BG_BLUE}◤ " # end USERNAME container / begin DIRECTORY container
PS1+="${FG_GREY}\w " # print directory
PS1+="${FMT_RESET}${FG_BLUE}"
PS1+="\$(git branch 2> /dev/null | grep '^*' | colrm 1 2 | xargs -I BRANCH echo -n \"" # check if git branch exists
PS1+="${BG_YELLOW}◤" # end FILES container / begin BRANCH container
PS1+="${FG_WHITE}${FMT_BOLD} BRANCH " # print current git branch
PS1+="${FMT_RESET}${FG_YELLOW}\")◤\n" # end last container (either FILES or BRANCH)
PS1+="${FG_CYAN}\\$ " # print prompt
PS1+="${FMT_RESET}"
export PS1

source <(kubectl completion bash)
